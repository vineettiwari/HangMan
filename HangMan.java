/**
    HangMan.java - An MVP implementation of the Hang Man game in Java.
**/

public class HangMan {

    private String mWord;

    public HangMan(String word) {
        mWord = word;
    }

    public String getWord() {
        return mWord;
    }
}

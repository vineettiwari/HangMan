/**
    HMGame.java - The entry point to the Hang Man game.
**/

import java.io.Console;

public class HMGame {

    public static void main(String[] args) {
        Console console = System.console();
        console.printf("\n");

        String word = console.readLine("Enter a word:  ");
        HangMan hangMan = new HangMan(word);

        word = hangMan.getWord();
        console.printf("Word:  %s\n", word);

        console.printf("\n");
    }
}
